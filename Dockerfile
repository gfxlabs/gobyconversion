FROM golang:buster
run mkdir -p /wd
WORKDIR /wd
run cd /wd 
COPY ./ /wd/
run /wd/tools/build


EXPOSE 8000
CMD ["/wd/tools/serve"]
