<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Go by Example: Command-Line Arguments</title>
    <link rel=stylesheet href="site.css">
  </head>
  <script>
      onkeydown = (e) => {
          
          if (e.key == "ArrowLeft") {
              window.location.href = 'testing-and-benchmarking';
          }
          
          
          if (e.key == "ArrowRight") {
              window.location.href = 'command-line-flags';
          }
          
      }
  </script>
  <body>
    <div class="example" id="command-line-arguments">
      <h2><a href="./">Go by Example</a>: Command-Line Arguments</h2>
      <table>
      <tbody>
      <tr>
      <td>
      
      <table>
        
        <tr>
          
          
        
        <tr>
          
          <td class="docs">
            
          </td>
          <td class="code leading">
            <a href="http://play.golang.org/p/oSxtj7v_v1K"><img title="Run code" src="play.png" class="run" /></a><img title="Copy code" src="clipboard.png" class="copy" />
          <pre class="chroma"><span class="kn">package</span> <span class="nx">main</span>
</pre>
          </td>
          
          
        
        <tr>
          
          <td class="docs">
            
          </td>
          <td class="code leading">
            
          <pre class="chroma"><span class="kn">import</span> <span class="p">(</span>
    <span class="s">&#34;fmt&#34;</span>
    <span class="s">&#34;os&#34;</span>
<span class="p">)</span>
</pre>
          </td>
          
          
        
        <tr>
          
          <td class="docs">
            
          </td>
          <td class="code leading">
            
          <pre class="chroma"><span class="kd">func</span> <span class="nf">main</span><span class="p">()</span> <span class="p">{</span>
</pre>
          </td>
          
          
        
        <tr>
          
          <td class="docs">
            <p><code>os.Args</code> provides access to raw command-line
arguments. Note that the first value in this slice
is the path to the program, and <code>os.Args[1:]</code>
holds the arguments to the program.</p>

          </td>
          <td class="code leading">
            
          <pre class="chroma">
    <span class="nx">argsWithProg</span> <span class="o">:=</span> <span class="nx">os</span><span class="p">.</span><span class="nx">Args</span>
    <span class="nx">argsWithoutProg</span> <span class="o">:=</span> <span class="nx">os</span><span class="p">.</span><span class="nx">Args</span><span class="p">[</span><span class="mi">1</span><span class="p">:]</span>
</pre>
          </td>
          
          
        
        <tr>
          
          <td class="docs">
            <p>You can get individual args with normal indexing.</p>

          </td>
          <td class="code leading">
            
          <pre class="chroma">
    <span class="nx">arg</span> <span class="o">:=</span> <span class="nx">os</span><span class="p">.</span><span class="nx">Args</span><span class="p">[</span><span class="mi">3</span><span class="p">]</span>
</pre>
          </td>
          
          
        
        <tr>
          
          <td class="docs">
            
          </td>
          <td class="code">
            
          <pre class="chroma">    <span class="nx">fmt</span><span class="p">.</span><span class="nf">Println</span><span class="p">(</span><span class="nx">argsWithProg</span><span class="p">)</span>
    <span class="nx">fmt</span><span class="p">.</span><span class="nf">Println</span><span class="p">(</span><span class="nx">argsWithoutProg</span><span class="p">)</span>
    <span class="nx">fmt</span><span class="p">.</span><span class="nf">Println</span><span class="p">(</span><span class="nx">arg</span><span class="p">)</span>
<span class="p">}</span>
</pre>
          </td>
          
          
        
      </table>
      <table>
        
        <tr>
          
          
          <td class="docs">
            <p>To experiment with command-line arguments it&rsquo;s best to
build a binary with <code>go build</code> first.</p>

          </td>
          <td class="code leading">
            
          <pre class="chroma">
<span class="gp">$</span> go build command-line-arguments.go
<span class="gp">$</span> ./command-line-arguments a b c d
<span class="go">[./command-line-arguments a b c d]       
</span><span class="go">[a b c d]
</span><span class="go">c</span></pre>
          </td>
          </tr>
          
        
        <tr>
          
          
        
      </table>
      </td>
      <td>
      javascript:
       
      
          <table>
            
              
            
              
            
              
            
              
            
              
            
              
            
              
            
          </table>
          
       
      
          <table>
            
              
            
              
            
          </table>
          
      
      </td>
      </tr>
      </tbody>
      </table>

      <p class="next">
        Next example: <a href="command-line-flags">Command-Line Flags</a>.
      </p>
      
      </td>
      </tr>
      </tbody>
    </table>

    <p class="footer">
      by <a href="https://markmcgranaghan.com">Mark McGranaghan</a> and <a href="https://eli.thegreenplace.net">Eli Bendersky</a> | <a href="https://github.com/mmcgrana/gobyexample">source</a> | <a href="https://github.com/mmcgrana/gobyexample#license">license</a>
    </p>

    </div>
    <script>
      var codeLines = [];
      codeLines.push('');codeLines.push('package main\u000A');codeLines.push('import (\u000A    \"fmt\"\u000A    \"os\"\u000A)\u000A');codeLines.push('func main() {\u000A');codeLines.push('    argsWithProg :\u003D os.Args\u000A    argsWithoutProg :\u003D os.Args[1:]\u000A');codeLines.push('    arg :\u003D os.Args[3]\u000A');codeLines.push('    fmt.Println(argsWithProg)\u000A    fmt.Println(argsWithoutProg)\u000A    fmt.Println(arg)\u000A}\u000A');codeLines.push('');codeLines.push('');
    </script>
    <script src="site.js" async></script>
  </body>
</html>
