

const m = new Map()

m.set("k1", 7)
m.set("k2", 13)

console.log("map:", m)

// Get a value for a key with `name[key]`.
const v1 = m["k1"]
console.log("v1: ", v1)

console.log("len:", len(m))

m.delete("k2")
console.log("map:", m)

let [_, prs] = [m.get("k2"), m.has("k2")]
console.log("prs:", prs)

let n = new Map(Object.entries({"foo": 1, "bar": 2}))
console.log("map:", n)

