
class person {
	constructor(name, age) {
		this.name = name
		this.age = age
	}
}

const newPerson = (name, age) => {
	if(age == 0){
		age = 34
	}
	return new person(name, age)
}


console.log({"name":"Bob", "age": 20})

console.log(newPerson("Jon",0))

let s = newPerson("Sean", 50)
console.log(s.name)

s.age = 51
console.log(s.age)
