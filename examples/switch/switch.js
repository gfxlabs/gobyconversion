const i = 2
console.log("Write ", i, " as ")
switch(i){
case 1:
	fmt.Println("one")
	break; // go is no fallthrough by default
case 2:
	fmt.Println("two")
	break; 
case 3:
	fmt.Println("three")
	break; 
}

// You can use commas to separate multiple expressions
// in the same `case` statement. We use the optional
// `default` case in this example as well.
switch ((new Date()).getDay()) {
case 6, 7:
	console.log("It's the weekend")
	break;
default:
	console.log("It's a weekday")
	break;
}