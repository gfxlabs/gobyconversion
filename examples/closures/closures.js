const intSeq = ()=> {
	let i = 0
	return ()=> {
		i++
		return i
	}
}


// We call `intSeq`, assigning the result (a function)
// to `nextInt`. This function value captures its
// own `i` value, which will be updated each time
// we call `nextInt`.
const nextInt = intSeq()

// See the effect of the closure by calling `nextInt`
// a few times.
console.log(nextInt())
console.log(nextInt())
console.log(nextInt())

// To confirm that the state is unique to that
// particular function, create and test a new one.
const newInts = intSeq()
console.log(newInts())
