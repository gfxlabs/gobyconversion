
// Here we create an array `a` that will hold exactly
// 5 `int`s. The type of elements and length are both
// part of the array's type. By default an array is
// zero-valued, which for `int`s means `0`s.
const a = new Array(5)
console.log("emp:", a)

// zero-valued, which for `int`s means `0`s.
// We can set a value at an index using the
// `array[index] = value` syntax, and get a value with
// `array[index]`.
a[4] = 100
console.log("set:", a)
console.log("get:", a[4])

// The builtin `len` returns the length of an array.
console.log("len:", a.length)

// Use this syntax to declare and initialize an array
// in one line.
const b = [1, 2, 3, 4, 5]
console.log("dcl:", b)

// Array types are one-dimensional, but you can
// compose types to build multi-dimensional data
// structures.
const twoD = [new Array(3), new Array(3)]
for(let i = 0; i < 2; i++){
	for(j = 0; j < 3; j++ ){
		twoD[i][j] = i + j
	}
}
console.log("2d: ", twoD)
