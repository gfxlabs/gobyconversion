
// Here's the `string` we'll encode/decode.
const data = "abc123!?$*&()'-=@~"

// Decoding may return an error, which you can check
// if you don't already know the input to be
// well-formed.
try{
const sDec = atob(data)
console.log(sDec)
}catch(e){
console.log(e)
}

// This encodes/decodes using a URL-compatible base64
// format.
const uEnc = btoa(data)
console.log(uEnc)
const uDec = atob(uEnc)
console.log(uDec)
