const sum = (...nums) => {
	console.log(nums, " ")
	let total = 0
	for([_, num] of Object.entries(nums)) {
		total += num
	}
	console.log(total)
}


// Variadic functions can be called in the usual way
// with individual arguments.
sum(1, 2)
sum(1, 2, 3)

// If you already have multiple args in a slice,
// apply them to a variadic function using
// `func(slice...)` like this.
let nums = [1, 2, 3, 4]
sum(...nums)

