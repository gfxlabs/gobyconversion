
// The most basic type, with a single condition.
for(let i = 1; i <= 3; i++){
	console.log(i)
	i = i + 1
}

// A classic initial/condition/after `for` loop.
for(let j = 7; j <= 9; j++){
	console.log(j)
}

// `for` without a condition will loop repeatedly
// until you `break` out of the loop or `return` from
// the enclosing function.
while(true){
	console.log("loop")
	break
}

// You can also `continue` to the next iteration of
// the loop.
for(let n = 0; n <= 5; n++){
	if(n%2 == 0){
		continue
	}
	console.log(n)
}
