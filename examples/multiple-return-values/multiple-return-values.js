const vals = () => {
	return [3, 7]
}


// Here we use the 2 different return values from the
// call with _multiple assignment_.
let [a, b] = vals()
console.log(a)
console.log(b)

// If you only want a subset of the returned values,
// use the blank identifier `_`.
let [_, c] = vals()
console.log(c)
