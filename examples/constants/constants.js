// Go supports _constants_ of character, string, boolean,
// and numeric values.
// `const` declares a constant value.
const s = "constant"

const main = () => {
	console.log(s)

	const n = 500000000

	const d = 3e20 / n

	console.log(d)

	console.log(d)

	console.log(Math.sin(n))
}

main()