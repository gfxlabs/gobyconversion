// their sum as an `int`.
const plus = (a, b) => {
	// Go requires explicit returns, i.e. it won't
	// automatically return the value of the last
	// expression.
	return a + b
}

// When you have multiple consecutive parameters of
// the same type, you may omit the type name for the
// like-typed parameters up to the final parameter that
// declares the type.
const plusPlus = (a, b, c) => {
	return a + b + c
}

// Call a function just as you'd expect, with
// `name(args)`.
res = plus(1, 2)
console.log("1+2 =", res)

res = plusPlus(1, 2, 3)
console.log("1+2+3 =", res)