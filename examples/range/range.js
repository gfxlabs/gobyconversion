let nums = [2, 3, 4]
let sum = 0
for(num of nums){
	sum += num
}
console.log("sum:", sum)

// `range` on arrays and slices provides both the
// index and value for each entry. Above we didn't
// need the index, so we ignored it with the
// blank identifier `_`. Sometimes we actually want
// the indexes though.
for(let [idx, num] of Object.entries(nums)) {
	if(num == 3 ){
		console.log("index:", idx)
	}
}

const kvs = new Map(
	Object.entries({"a": "apple", "b": "banana"})
)
for(let [k, v] of Object.entries(kvs)){
	fmt.Printf("%s -> %s\n", k, v)
}

for(let k of Object.keys(kvs)){
	console.log("key:", k)
}

// `range` on strings iterates over Unicode code
// points. The first value is the starting byte index
// of the `rune` and the second the `rune` itself.
// See [Strings and Runes](strings-and-runes) for more
// details.
for(let [i, c] of Object.entries(Array.from(kvs))){
	console.log(i, c)
}
